LISP ?= sbcl
PROJECT ?= havoc
ENTRY_POINT ?= main

test: eval

eval:
	$(LISP) --eval '(ql:quickload :$(PROJECT))' \
		--eval '($(PROJECT)::$(ENTRY_POINT))'

build:
	$(LISP) --load $(PROJECT).asd \
		--eval '(ql:quickload :$(PROJECT))' \
		--eval '(asdf:make :$(PROJECT))' \
		--eval '(quit)'

run:
	./$(PROJECT)
