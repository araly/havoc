;;;; havoc.asd
(asdf:defsystem :havoc
  :description "bot written in lisp"
  :author "araly"
  :maintainer "araly"
  :license "do what you want but mention the original project, and keep that same licence"
  :serial t
  :depends-on (:lispcord)
  :components
  ((:module src
    :serial t
    :components
            ((:file "package")
             (:file "commands")
             (:file "main")))))
