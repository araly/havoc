;;;; src/main.lisp, creates the bot and runs it
(in-package :havoc)
(set-log-level :info)
(defbot *bot* "NDQ2MDI4NTc3MzE0NzAxMzUy.XcNdPw.m_xLcyKzlEyWrK-TpSodtCIM9IQ")

(defun message-create (msg)
  "if the command is allowed to be understood, sends it to be understood"
  (cond ((lc:botp (lc:author msg)) nil)
        ((not (starts-with (lc:content msg) *prefix*)) nil)
        (t (dispatch-commands msg))))

(add-event-handler :on-message-create #'message-create)

(add-event-handler :on-ready
                   (lambda (ready)
                     (format t "User: ~a~%Session: ~a~%Connected~%" (lc:name (lc:user ready)) (lc:session-id ready))))

(defun main ()
  (connect *bot*))

(main)

