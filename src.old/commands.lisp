;;;; src/commands.lisp, commands and essential functions for understanding commands
(in-package :havoc)

;;;Core
(defparameter *prefix* "&")

(defparameter *commands* (make-hash-table :test #'equal))

(defun split (string &aux (last 0))
  "Splits a string at white space"
  (append (loop :for e :across string
                :counting e :into c
                :if (member e '(#\space #\newline #\tab))
                :collect (prog1 (subseq string last (1- c))
                           (setf last c)))
          (list (subseq string last))))

(defun dispatch-commands (message)
  "processes the command and looks it up in *commands*"
  (let* ((no-prefix (string-trim *prefix* (lc:content message))))
    (destructuring-bind (cmd &rest args) (split no-prefix)
      (cond ((not (gethash (string-downcase cmd) *commands*)) nil)
            (t (apply (gethash (string-downcase cmd) *commands*) message args))))))

(defmacro defcommand (command args &body body)
  "Constructs and registers a bot-command in *commands*"
  `(setf (gethash ,(string-downcase (string command)) *commands*)
         (lambda ,args ,@body)))

(defun starts-with (string start-sequence)
  "returns non-nil if string S starts with BEGINS"
  (cond ((>= (length string) (length start-sequence))
         (string-equal (subseq string 0 (length start-sequence)) start-sequence))
        (t nil)))

;;;Methods
#||
(defun delimiterp (c)
  (or (char= c #\Space) (char= c #\,) (char= c #\.)))

(defun split-into-words (string &key (delimiterp #'delimiterp))
  "split string into list of strings"
  (loop :for beg = (position-if-not delimiterp string)
        :then (position-if-not delimiterp string :start (1+ end))
        :for end = (and beg (position-if delimiterp string :start beg))
        :when beg :collect (subseq string beg end)
        :while end))
||#

(defun btc-price ()
  "reads JSON from an API with CURL, reads through it and returns btc price"
  (let ((outstring (uiop:run-program "curl https://api.coindesk.com/v1/bpi/currentprice.json" :output :string))
        (tag "rate\":\""))
    (remove #\( (remove #\) (remove #\  (format nil "~a" (loop :for e :across outstring
                                                               :counting e :into c
                                                               :with i = -1
                                                               :until (or (equal i -2) (>= c (- (length outstring) (length tag))))
                                                               :if (and (>= i 0) (>= c i) (equal e #\"))
                                                               :do (setq i -2)
                                                               :if (and (equal i -1) (equal tag (subseq outstring c (+ c (length tag)))))
                                                               :do (setq i (+ c (length tag) 1)))))))))

(defun read-rp-characters (path)
  "reads path and returns a list of instances of rp-character"
  (with-open-file (stream path)
    (loop :for line = (read-line stream nil)
          :while line
          :if (not (starts-with line ";"))
          :collect (loop :for parameter :in (split line)
                         :collect (let ((colon-index (loop :for character :across parameter
                                                           :counting character :into index
                                                           :if (equal character #\:)
                                                           :do (return (- index 1)))))
                               (if colon-index
                                   (list (subseq parameter 0 colon-index) (subseq parameter (+ 1 colon-index)))))))))

(defun rp-character-value (rp-character parameter-name)
  "returns the value of a parameter of a rp-character"
  (loop :for parameter :in rp-character
        :if (equal (car parameter) parameter-name)
        :do (return (cadr parameter))))

(defun command-list ()
  "generates and returns a list of the commands in the *commands* hash table in a string"
  (loop :for key :being :the :hash-key :of *commands*
        :with result = ""
        :do (setq result (format nil "~a**~a~a:**~%" result *prefix* key))
        :finally (return result)))

(defun dispatch-rolls (text msg) ;to clean and comment
  "understands the text as a roll and returns a result depending on what roll it was, special for 2d6"
  (cond ((equal (length text) (nth-value 1 (parse-integer text :junk-allowed t)));text strictly a number, rolling that number
         (format nil "`~a:` **~a**" text (+ 1 (random (parse-integer text)))))
        ((or (equal text "dex") (equal text "str") (equal text "con") (equal text "int") (equal text "wis") (equal text "cha") (equal text "men") (equal text "per")) ;text a parameter of a character, rolling 2d6+modifier of the parameter
         (format nil "`~a:` **~a**" text (let ((modifier (rp-character-value (loop :for rp-character :in (read-rp-characters "/home/araly/Documents/araly/lisp/havoc/src/characters")
                                                                                   :if (and (equal (rp-character-value rp-character "user") (lc:name (lc:author msg)))
                                                                                            (equal (rp-character-value rp-character "guild") (lc:name (lc:guild msg))))
                                                                                   :do (return rp-character))
                                                                             text)))
                                           (cond ((= 0 (length modifier))
                                                  "error: no ~a parameter for a character for ~a in this guild" text (lc:name (lc:author msg)))
                                                 (t
                                                  (let ((result (+ (loop :for i :from 1 :to 1
                                                                         :sum (+ 1 (random 20)))
                                                                   (parse-integer modifier))))
                                                    (cond ((<= result (+ 1 (parse-integer modifier)))
                                                           (setq result (format nil "critical failure")))
                                                          ((< result (+ 20 (parse-integer modifier)))
                                                           result)
                                                          (t
                                                           (setq result (format nil "critical success"))))))))))
        (t;else
         (let ((number-of-dice 0)
               (die-type 0)
               (modifier 0)
               (d-index (loop :for character :across text;searching for "d"
                              :counting character :into index
                              :if (equal character #\d)
                              :do (return (- index 1))))
               (+-index (loop :for character :across text;searching for "+" or "-"
                              :counting character :into index
                              :if (or (equal character #\-) (equal character #\+))
                              :do (return (- index 1))))
               (result 0))
           (if (or (not d-index) (equal d-index 0));error handling
               (return-from dispatch-rolls (format nil "`~a:` **error: \"d\" not found**" text)))
           (if (not +-index)
               (setq +-index (length text)))
           (if (< +-index d-index)
               (return-from dispatch-rolls (format nil "`~a:` **error: \"+\" or \"-\" before \"d\"**" text)))
           (if (= d-index (nth-value 1 (parse-integer (subseq text 0 d-index) :junk-allowed t)))
               (setq number-of-dice (parse-integer (subseq text 0 d-index)))
               (return-from dispatch-rolls (format nil "`~a:` **error: no strict number found before \"d\"**" text)))
           (if (= (- +-index d-index 1) (nth-value 1 (parse-integer (subseq text (+ 1 d-index) +-index) :junk-allowed t)))
               (setq die-type (parse-integer (subseq text (+ 1 d-index) +-index)))
               (return-from dispatch-rolls (format nil "`~a:` **error: no strict number found before \"+\" or \"-\"**" text)))
           (if (not (= (length text) +-index))
               (if (= (- (length text) +-index) (nth-value 1 (parse-integer (subseq text +-index) :junk-allowed t)))
                   (setq modifier (parse-integer (subseq text +-index)))
                   (return-from dispatch-rolls (format nil "`~a:` **error: no strict number found after \"+\" or \"-\"**" text))))

           (setq result (+ (loop :for i :from 1 :to number-of-dice;rolling dice
                                 :sum (+ 1 (random die-type)))
                           modifier))
           (if (and (= number-of-dice 2) (= die-type 6));if of type 2d6, understand the roll
               (cond ((<= result 2)
                      (setq result (format nil "critical failure (~a)" result)))
                     ((<= result 6)
                      (setq result (format nil "failure (~a)" result)))
                     ((<= result 9)
                      (setq result (format nil "neutral (~a)" result)))
                     ((<= result 11)
                      (setq result (format nil "success (~a)" result)))
                     (t
                      (setq result (format nil "critical success (~a)" result)))))
           (format nil "`~a:` **~a**" text result)))));else, just give the result of the roll

;;classes for the voting system

(defclass vote-session ()
  ((guild :accessor vote-session-guild
          :initarg :guild)
   (candidates :accessor vote-session-candidates
               :initarg :candidates)
   (voters :accessor vote-session-voters
           :initarg :voters)))

(defclass candidate ()
  ((name :accessor candidate-name
         :initarg :name)
   (score :accessor candidate-score
          :initarg :score)))

(defclass voter ()
  ((name :accessor voter-name
         :initarg :name)
   (vote :accessor voter-vote
         :initarg :vote)
   (confidence :accessor voter-confidence
               :initarg :confidence)))

(defvar *session* (make-instance 'vote-session) "session for the voting")

(defun display-vote-session (session)
  "display a resume of the vote session, focused on the candidates and scores, through text"
  (format nil "voting session~%~a" (loop :for candidate :in (vote-session-candidates session)
                                         :with result = ""
                                         :do (setf result (format nil "~a~%*~a:* **~$**" result (candidate-name candidate) (candidate-score candidate)))
                                         :finally (return result))))

;;;Commands
(defcommand help (msg)
  "help menu"
  (reply msg (format nil "I'm a bot written by Araly with the help of the lispcord library https://github.com/lispcord/lispcord~%~a" (command-list))))

(defcommand roll (msg &rest body)
  "rolls using dispatch-rolls"
  (let ((content (if body
                     (loop :for roll :in body
                           :with result = ""
                           :if (not (equal (string-downcase roll) "private"))
                           :do (setq result (format nil "~a~a~%" result (dispatch-rolls roll msg)))
                           :finally (return result))
                     (write-to-string (loop :for i :from 1 :to 1
                                            :sum (+ 1 (random 20)))))))
    (if (equal (string-downcase (car body)) "private")
        (progn (create (format nil "private ~a" content) (lc:author msg))
               (loop :for member :across (lc:members (lc:guild (lc:channel msg)))
                     :do (loop :for role :across (lc:roles member)
                               :if (or (equal (lc:name role) "Game Master") (equal (lc:name role) "Masta"))
                               :do (create (format nil "private for ~a: ~a" (lc:name (lc:author msg)) content) (lc:user member)))))
        (reply msg (format nil "~a: ~a" (mention (lc:author msg)) content)))))

(defcommand btc (msg)
  "prints btc price in usd"
  (reply msg (format nil "BTC: ~a USD" (btc-price))))

#||
(defcommand thw (msg &rest words)
  (reply msg (format nil "~a" (let ((result "illegal words: "))
                                (loop :for i :in (split-into-words words)
                                      :do (if (equal nil (find i (split-into-words (uiop:run-program "curl splasho.com/upgoer5/phpspellcheck/dictionaries/1000.dicin" :output :string))))
                                              (setq result (concatenate result " " i)))
                                      :finally (return result))))))
||#

(defcommand dev-info (msg)
  "prints guild id"
  (reply msg (format nil "guild: ~a~%user: ~a" (lc:id (lc:guild msg)) (lc:id (lc:author msg)))))

(defcommand char (msg)
  "prints list of characters"
  (reply msg (format nil "~a" (loop :for rp-character :in (read-rp-characters "/home/araly/Documents/araly/lisp/havoc/src/characters")
                                    :with result = (format nil "*Characters:*~%")
                                    :if (equal (rp-character-value rp-character "guild") (lc:name (lc:guild msg)))
                                    :do (setq result (format nil "~a~a~%" result (loop :for parameter :in rp-character
                                                                                       :with result2 = ""
                                                                                       :do (setq result2 (format nil "~a~%~a: **~a**" result2 (car parameter) (cadr parameter)))
                                                                                       :finally (return result2))))
                                    :finally (return result)))))

#||
(defcommand vote (msg &rest body)
  "initializes a vote and lets people vote in a dynamic fashion, altering the balance, until a candidate has all the points"
  (cond ((equal body nil)
         (reply msg (format nil "help board, to come later")))
        ((equal (car body) "init")
         (progn (setf *session* (make-instance 'vote-session
                                               :guild (lc:id (lc:guild msg))
                                               :candidates (loop :for word :in (cdr body)
                                                                 :collect (make-instance 'candidate
                                                                                         :name word
                                                                                         :score (/ 100 (list-length (cdr body)))))))
                (with-open-file (stream "~/Documents/lisp/havoc/src/vote"
                                        :direction :output
                                        :if-exists :supersede
                                        :if-does-not-exist :create)
                  (format stream ";;;;VOTE SESSION ~a#~a~%~a" (lc:name (lc:guild msg)) (lc:id (lc:guild msg)) (format nil "~{~a~^ ~}" (loop :for candidate :in (vote-session-candidates *session*)
                                                                                                                                            :collect (candidate-name candidate)))))
                (reply msg (display-vote-session *session*))))
        ((not (equal (vote-session-candidates *session*) nil))
         (if (equal (vote-session-guild *session*) (lc:id (lc:guild msg)))
             (let ((voter (loop :for voter :in (vote-session-voters *session*)
                                :with result = nil
                                :if (equal (voter-name voter) (lc:name (lc:author msg)))
                                :do (setf result t)
                                :finally (return result))))
               (if (equal voter nil)
                   (make-instance 'voter :name (lc:name (lc:author msg)) :)))))))
||#
